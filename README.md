Spacious Skies Woodland Hills Campground is a family-friendly campground located in upstate New York amidst the Taconic and Berkshire mountain ranges, set between Albany and Pittsfield, MA, and only two hours north of New York City.
|| Address: 386 Fog Hill Rd, Austerlitz, NY 12017, USA || Phone: 518-392-3557 || Website: https://campatwoodlandhills.com
